package pl.aptewicz.sda.java.oop.domain;

import java.util.Objects;

public abstract class Car {
    // access_modifier type name;
    protected String color;
    protected String brand;
    protected short engineCapacity;
    protected short yearOfProduction;
    protected byte numberOfDoors;
    protected String typeOfFuel;
    protected boolean isOk;

    public Car(String color, String brand, short engineCapacity, short yearOfProduction, byte numberOfDoors,
               String typeOfFuel, boolean isOk) {
        this.color = color;
        this.brand = brand;
        this.engineCapacity = engineCapacity;
        this.yearOfProduction = yearOfProduction;
        this.numberOfDoors = numberOfDoors;
        this.typeOfFuel = typeOfFuel;
        this.isOk = isOk;
    }

    // access_modifier return_type name(args) {}
    public boolean drive() {
        if (isOk) {
            System.out.println("Car is driving...");
            giveDriveSound();
            return isOk;
        }
        System.out.println("Car is not driving...");
        return isOk;
    }

    public abstract void giveDriveSound();

    protected String describe() {
        return "Car brand is: " + this.brand + "\n" +
                "color is: " + this.color + "\n" +
                "engineCapacity is: " + this.engineCapacity + "\n" +
                "yearOfProduction is: " + this.yearOfProduction + "\n" +
                "numberOfDoors is: " + this.numberOfDoors + "\n" +
                "typeOfFuel is: " + this.typeOfFuel + "\n" +
                "isOk is: " + this.isOk + "\n";
    }

    public void openDoors() {
        System.out.println("Doors are open...");
    }

    public void closeDoors() {
        System.out.println("Doors are closed...");
    }

    public boolean changeGear(byte gearNumber) {
        return this.isOk;
    }

    // setter
    public void setOk(boolean isOk) {
        this.isOk = isOk;
    }

    // getter
    public boolean getIsOk() {
        return this.isOk;
    }

    public String getBrand() {
        return this.brand;
    }

    @Override
    public String toString() {
        return this.describe();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Car))
            return false;
        Car car = (Car) o;
        return engineCapacity == car.engineCapacity && yearOfProduction == car.yearOfProduction &&
                numberOfDoors == car.numberOfDoors && isOk == car.isOk && Objects.equals(color, car.color) &&
                Objects.equals(brand, car.brand) && Objects.equals(typeOfFuel, car.typeOfFuel);
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, brand, engineCapacity, yearOfProduction, numberOfDoors, typeOfFuel, isOk);
    }
}
