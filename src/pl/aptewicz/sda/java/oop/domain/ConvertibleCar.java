package pl.aptewicz.sda.java.oop.domain;

public class ConvertibleCar extends Car {
    private boolean isRoofOpen;

    public ConvertibleCar(String color, String brand, short engineCapacity, short yearOfProduction, String typeOfFuel,
                          boolean isOk) {
        // always in first line of constructor code!!!
        super(color, brand, engineCapacity, yearOfProduction, (byte) 2, typeOfFuel, isOk);
    }

    public void openRoof() {
        if (!isRoofOpen) {
            System.out.println("Roof is opening...");
            this.isRoofOpen = true;
        } else {
            System.out.println("Roof is already open...");
        }
    }

    public void closeRoof() {
        if (isRoofOpen) {
            System.out.println("Roof is closing...");
            this.isRoofOpen = false;
        } else {
            System.out.println("Roof is already closed...");
        }
    }

    @Override
    protected String describe() {
        return super.describe() + "isRoofOpen: " + isRoofOpen + "\n";
    }

    @Override
    public void giveDriveSound() {
        System.out.println("Convertible car brum brum...");
    }
}
