package pl.aptewicz.sda.java.oop.domain;

public class Truck extends Car {
    public Truck(String color, String brand, short engineCapacity, short yearOfProduction, byte numberOfDoors,
                 String typeOfFuel, boolean isOk) {
        super(color, brand, engineCapacity, yearOfProduction, numberOfDoors, typeOfFuel, isOk);
    }

    @Override
    public void giveDriveSound() {
        System.out.println("Truck brum brum...");
    }
}
