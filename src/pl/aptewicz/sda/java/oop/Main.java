package pl.aptewicz.sda.java.oop;

import pl.aptewicz.sda.java.oop.domain.Car;
import pl.aptewicz.sda.java.oop.domain.ConvertibleCar;
import pl.aptewicz.sda.java.oop.domain.Truck;

public class Main {

    public static void main(String[] args) {
        final Car truck = new Truck(
                "red",
                "Scania",
                (short) 2500,
                (short) 2010,
                (byte) 3,
                "Gasolin",
                true
        );

        final ConvertibleCar proshe = new ConvertibleCar(
                "black",
                "porshe",
                (short) 2500,
                (short) 2010,
                "Gasolin",
                true
        );

        final Car anonymousCar = new Car("red",
                "Scania",
                (short) 2500,
                (short) 2010,
                (byte) 3,
                "Gasolin",
                true) {

            @Override
            public void giveDriveSound() {
                System.out.println("Anonymous car brum brum...");
            }
        };

        final Car[] cars = {
                truck,
                proshe,
                anonymousCar
        };

        for (Car car: cars) {
            System.out.println(car);
            car.drive();
            System.out.println(car.getClass().getSimpleName());
            System.out.println();
        }

//        equalsLearning();
    }

    /*private static void equalsLearning() {
        final Car car1 = new Car("black", "BMW", (short) 2000, (short) 1995, (byte) 5, "Diesel", true);
        final Car car2 = new Car("black", "BMW", (short) 2000, (short) 1995, (byte) 5, "Diesel", true);

        if (car1 == car2) {
            System.out.println("car1 == car2");
        } else {
            System.out.println("car1 != car2");
        }

        if (car1.equals(car2)) {
            System.out.println("car1 is equal to car2");
        } else {
            System.out.println("car1 is not equal to car2");
        }

        final String a = null;

        if ("b".equals(a)) {
            System.out.println("strings are the same");
        }
    }*/
}
